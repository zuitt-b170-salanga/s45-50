/* s45-50=pages ---> Register.js*/

/*
	import:
		react, useState, useEffect from react

		Form, Container, Button from react-bootstrap
*/

// Base Imports
import React, {useState, useEffect, useContext} from 'react';
import { Navigate } from 'react-router-dom';
// ******************s49**********************
// App Imports
import UserContext from '../userContext'

// Bootstrap
import {Form, Container, Button} from 'react-bootstrap';
import Swal from 'sweetalert2'; // npm install sweetalert2 to install sweetalert2


export default function Register() {

	// *****************s49********************
	const { user, setUser } = useContext(UserContext);
	// *************************************

	// ******************s50*******************
	const[firstName, setFirstName] = useState('');
	const[lastName, setLastName] = useState('');
	const[mobileNo, setMobileNo] = useState('');
	// ******************s50*******************
	const[email, setEmail] = useState('');
	const[password, setPassword] = useState('');
	const[passwordConfirm, setPasswordConfirm] = useState('');
	const[isDisabled, setIsDisabled] = useState(true);

	useEffect(()=>{
		// ******************s50*******************
		let isFirstNameNotEmpty = firstName !== '';
		let isLastNameNotEmpty = lastName !== '';
		let isMobileNoNotEmpty = mobileNo !== '';
		// ******************s50*******************
		let isEmailNotEmpty = email !== '';
		let isPasswordNotEmpty = password !== '';
		let isPasswordConfirmNotEmpty = passwordConfirm !== '';
		let isPasswordMatch = password === passwordConfirm;

		// ******************s50*******************
		if (isMobileNoNotEmpty && isLastNameNotEmpty && isFirstNameNotEmpty && isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatch) {
		// ******************s50*******************
		/*if (isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatch) {*/
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	// ******************s50*******************	
	},[firstName, lastName, mobileNo, email, password, passwordConfirm]);

	/*
		TWO-WAY BINDING

			to be able to capture/save the input value from the input elements, we can bind the
			value of the element w/ the states. We, as devs. cannot type into the inputs anymore
			bec there is now value that is bound to it. We will add an onChange event to be able
			to update the state that is bound to the input.

			Two-way binding is done so that we can assure that we can save the input into our states
			as the users type into the element

			"e.target.value"
			e - the event to w/c the element will listen
			target - the element where the event will happen 
			value - the value that the user has entered in that element

	*/

	 // *****************s50************************
	/*function register(e){
		e.preventDefault();

		Swal.fire('Register successful, you may now log in.')*/

		//clears the input fields since they update their respective variable values into an empty string
		// ******************s50*******************	
	/*	setFirstName('');
		setLastName('');
		setMobileNo('');
	
		setEmail('');
		setPassword('');
		setPasswordConfirm('');
	}*/
	// *****************s50************************ 

    function register(e) {
    	e.preventDefault();

    	fetch('http://localhost:4000/api/users/checkEmail', {
    		method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify ({
            	email: email
            })
    	})
    	.then((response) => response.json())
        .then((response) => {
        	if (response.access !== undefined) {
        		Swal.fire('Email Unused')

        	localStorage.setItem('access', response.access);
                setUser( { access: response.access } );	
        		    		
        	} else {
        		alert(response.error);
        		setFirstName('');
				setLastName('');
				setMobileNo('');
	
				setEmail('');
				setPassword('');
				setPasswordConfirm('');
        	}
        })        	
	}



        fetch('http://localhost:4000/api/users/register', {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify( {
            	firstName: firstName,
            	lastName: lastName,
            	mobileNo: mobileNo,
                email: email, 
                password: password,
                passwordConfirm: passwordConfirm
                } )
        })
        .then((response) => response.json())
        .then((response) => {
            if (response.access !== undefined) {
                Swal.fire('Register successful, you may now log in');

                 // localStorage.setItem - to store a piece of data inside the localStorage of the browser. this is because the local storage does not delete unless it manually done so or the codes make it delete the information inside
                
                /*localStorage.setItem('access', response.access);
                setUser( { access: response.access } );*/

                setFirstName('');
				setLastName('');
				setMobileNo('');
                setEmail('');
                setPassword('');
                setPasswordConfirm('');
            }
            else {
                alert(response.error);
                
            }
        })
        if (user.access !== null) {
		return<Navigate replace to="/courses" />
	}	

       

    
	// *****************s49********************
	/*if (user.access !== null) {
		return<Navigate replace to="/courses" />
	}	*/
	// **********comment out if needed***************************



	return(

		<Container>
			<Form onSubmit={register}>
				{/*// *************s50*******************/}
				<Form.Group>
					<Form.Label>First Name</Form.Label>
					<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={(e)=>
						setFirstName(e.target.value)}required />			
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name</Form.Label>
					<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={(e)=>
						setLastName(e.target.value)}required />			
				</Form.Group>				
				<Form.Group>
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control type="tel" placeholder="Enter Mobile Number" maxlength="11" value={mobileNo} onChange={(e)=>
						setMobileNo(e.target.value)}required />			
				</Form.Group>
				
				{/*// ***************s50*****************/}
				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => 
						setEmail(e.target.value)} required />
					<Form.Text className="text-muted">We'll never share your email to anyone else</Form.Text>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter password" value={password} onChange={(e)=>
						setPassword(e.target.value)}required />			
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control type="password" placeholder="Confirm Password" value={passwordConfirm} onChange={(e)=>
						setPasswordConfirm(e.target.value)} required />					
				</Form.Group>
				<Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>

			</Form>
		</Container>
	)
}







