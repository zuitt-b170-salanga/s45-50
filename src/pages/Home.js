/* s45-50=pages ---> Home.js */

// dependencies
import	React, { Fragment } from 'react';


//App Components
import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';

export default function Home() {

	const data = {
	    title: "Zuitt Coding Bootcamp",
	    content: "Opportunities for everyone, everywhere",
	    destination: "/courses",
	    label: "Enroll now!"
	}

	return (
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}

/*export default function Home() {
	return (
		 <Container fluid>
    		<Banner/>
    		<Highlights/>
  		</Container>
		)
}*/

// ============================================
//for s48, refactor banner, home, error & app.js









