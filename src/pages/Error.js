/* s45-50=pages ---> Error.js*/

import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }
    
    return (
        <Banner data={data} />
    )
}


/*import React from 'react';
// *************************** 
import { Link } from 'react-router-dom';

// Bootstrap
import {Form, Container, Button} from 'react-bootstrap';

// ********try************  
//App Components
import Banner from '../components/Banner.js';
// ********try************   
  
export default function Error() {
	return (
		<Container>
			<div>
        		<h1>Page Not Found</h1>
        		<p>	
        		 	Go back to the
        			<Link to="/"> homepage</Link>
        			.
        		</p>

    		</div>
		</Container>
	)
}*/

// ============================================
//for s48, refactor banner, home, error & app.js













