/* s45-50=src:   index.js*/

/*
code to create react app (command in gitbash/terminal)
  npx create-react-app <appName>
*/

/*
  after installing, remove all files inside the src folder except index.js
  remove the README.md file in the root folder of the app
*/


// set up/import dependencies
import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import App from './App.js';


// App Components

/*
    every component has to be imported before it can be rendered inside the index.js
*/



/*
  ReactDOM.render()
    responsible for injecting/inserting the whole React.js Project inside the webpage
*/

/*
  deprecated version() of the render() in react
*/


// ReactDOM.render(<h1>Hello World</h1>,document.getElementById('root'));
// React.createElement('h1',null,'Hello World');


/*
  JSX - JavaScript XML - is an extension of JS that lets us create objects w/c will then be 
    compiled & added as HTML elements

  With JSX...
    - we are able to create HTML elements using JS
    - we are also able to create JS objects that will then be compiled & added as HTML elements
*/


//ReactDOM.render(<AppNavbar />, document.getElementById('root'));
//ReactDOM.render(<AppNavbar /><Banner/><Highlights/>, document.getElementById('root'));

/*
  Fragment - used to render diff components inside the index.js; w/o it, the webpage will return 
    errors since it is the of JS to display 2 or more components in the frontend
    <>
    </>
      - also accepted in place of the Fragment but not all browsers are able to read this. Also,
        this does not support keys or attributes
*/

/*
  npm start - to start/launch the react app
*/
ReactDOM.render(
  <App />
 ,document.getElementById('root')
);

























//*************************************
/*ReactDOM.render(

 <Fragment>
   <AppNavbar />
   <Login /> 

 </Fragment>, 
  
  document.getElementById('root')
);*/








