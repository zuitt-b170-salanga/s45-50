/* s45-50=src:   userContext.js */

// Base Imports
import React from 'react';

/*
	React.createContext()
		- create a Context Object
			- is a special obj that allows info storage w/in the app & pass it around the 
				components (like a moving flashdrive)

			w/ this, we'll be able to create global state to store user details instead of getting it 
				fr localStorage fr time to time

			a diff approach to passing info bet components w/o the use of props & pass it fr parent
				to child since it is already being passed on to other components as a global state for 
				the user
*/


export default React.createContext(); 








