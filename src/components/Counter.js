/* s45-50=components ---> Counter.js*/

import React, { useState, useEffect } from 'react';

import Container from 'react-bootstrap/Container';


export default function Counter() {
	const [ count, setCount ] = useState(0);

/*
	Effect hook -- allow us to execute a piece of code whenever a component gets rendered to the page 
		or if the value of a state changes.
			-the devs need useEffect for the page (or at least part of the page) to be reactive.
	useEffect -- requires 2 arguments: function & the array
			-function - to specify the codes to be executed
			-array - to set w/c variable is to be listened to, in terms of changing the state, for the 
				function to be executed. In some cases, not having any dependency array will cause the 
				page to have infinite loops; An empty array would let useEffect to execute the codes 
				only once 
*/	

	useEffect(() =>{	
			document.title = `You clicked ${count} times`;		
	},[])

	return(
			<Container fluid>
				<p>You clicked {count} times</p>
				<button onClick={() => setCount(count +1)}>Click me!</button>
			</Container>
		)
}










