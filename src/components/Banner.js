/* s45-50=components ---> Banner.js */

/*
	import the ff:
		React from react

		Jumbotron from react-bootstrap/Jumbotron
		Button from react-bootstrap/Button
		Row from react-bootstrap/Row
		Col from react-bootstrap/Col
*/

// Dependencies
import React from 'react';
import { Row,Col } from 'react-bootstrap';

import { Link } from 'react-router-dom';



/*// Bootstrap Components
import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';*/



export default function Banner({data}){

	console.log(data);
    const {title, content, destination, label} = data;

    return (
        <Row>
            <Col className="p-5">
                <h1>{title}</h1>
                <p>{content}</p>
                <Link to={destination}>{label}</Link>
            </Col>
        </Row>
    )
}


	/*return(
		<Row>
			<Col>
				<Jumbotron>
					<h1>Zuitt Coding Bootcamp</h1>
					<p>Opportunities for Everyone, Everywhere.</p>
					<Button variant="primary">Enroll Now!</Button>
				</Jumbotron>	
			</Col>
		</Row>
		)
}*/

// ============================================
//for s48, refactor banner, home, error & app.js
// banner meant to be reused by multiple pages










