/* s45-50=components --->  Highlights.js */

// dependencies
import React from 'react';

//Bootstrap Components
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


export default function Highlights() {
	return (
		<Row>
		{/*xs and md - allows setting of breakpoints*/}
			<Col xs={12} md={4}>
			<Card className="card-highlight">  
				<Card.Body>
					<Card.Title>
						<h2>Learn from Home</h2>
					</Card.Title>
					<Card.Text>
						Enjoy learning in the comfort of your room/home
					</Card.Text>
				</Card.Body>
			</Card>	
			</Col>
			

			
			<Col xs={12} md={4}>
			<Card className="card-highlight">  
				<Card.Body>
					<Card.Title>
						<h2>Study Now, Pay Later</h2>
					</Card.Title>
					<Card.Text>
						Never be bothered by the expenses while learning
					</Card.Text>
				</Card.Body>
			</Card>	
			</Col>


			<Col xs={12} md={4}>
			<Card className="card-highlight">  
				<Card.Body>
					<Card.Title>
						<h2>Be Part of Our Community</h2>
					</Card.Title>
					<Card.Text>
						Zuitt offers job for aspiring web developers from beginners to 
							experienced who wants to refresh their minds on coding
					</Card.Text>
				</Card.Body>
			</Card>	
			</Col>


		</Row>
		)
}

// ******************************************************************
// highlight meant to be used by 1 component bec of spec content


			