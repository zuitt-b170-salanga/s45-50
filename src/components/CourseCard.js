/* s45-50components ---> CourseCard.js */


// Base Imports
import React, {useState, useEffect} from 'react';


//Bootstrap Components
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';


export default function CourseCard(props) {

/*
	Props - short term for properties, similar to the arguments/parameters found inside the functions.
		A way for the parent component to receive info 

		through the use of props, devs can use the same component and feed different info/data for 
		rendering
*/

	let course = props.course

	/*
		useState() a hook used in React to allow components to create manage its own data & is meant to be 
			used internally.
				- accepts an argument that is meant to be the value of the 1st element in array
		
		in React.js, state values must not be changed directly. All changes to the state values must 
			be through the setState() function
				- setState() is the 2nd element in the created array variable

		the enrollees will start at zero, the result of the useState() is an array of data that is 
			destructured into count & setCount
		
		in the code below.....
			the enrollees will start at zero. the result of the useState() is an array of data that is 
			then destructured into count and setCount.

		setCount function is used to update the value of the count variable, depending on the times
			that the enroll function is triggered by the onClick command (button event)
	*/
	
	// ******************************

	/*
	const [ count, setCount ] = useState(0);   //setCount updates the value of count

	function enroll() {
		setCount (count + 1);  
	}
	*/
	// ******************************
	
	
	const [ isDisabled, setIsDisabled ] = useState(0);   
	const [ seats, setSeats ] = useState(30);


	useEffect(() => {
		if (seats === 0) {
			setIsDisabled(true);
		}
	},[seats]);

	// ******************************
	/*
	function enroll() {	
		
		if(seats === 0) {
			alert(`No more seats available.`); 
     
     	} else {
     		setCount (count + 1);
			setSeat ( seats - 1);
    	}	
	}
	*/

	// ******************************

	return (
					
			<Card>  
				<Card.Body>
					<Card.Title>{course.name}</Card.Title>
						<h6>Description:</h6>
						<p>{course.description}</p>
						<h6>Price:</h6>
						<p>{course.price}</p>
						<h6>Seats</h6>
						<p>{seats} remaining</p>
						<Button variant="primary" onClick={() => setSeats(seats-1)} disabled=
							{isDisabled}>Enroll</Button>			
				</Card.Body>
			</Card>	
		
		)
}


/* *************************************
export default function CourseCard() {
	return (
		<Row>
			<Col xs={12} md={14}>
			<Card className="card-courses">  
				<Card.Body>
					<Card.Title>
						<h3>Sample Course</h3>
					</Card.Title>
					<Card.Text>
						
						<h5>Description:</h5>

						This is a sample course offering.
					</Card.Text>
					<Card.Text>
						Price:
						PhP 40,000
					</Card.Text>
					
					<Button variant="primary">Enroll</Button>
					
				</Card.Body>
			</Card>	
			</Col>
		</Row>
		)
}


******************************************** */










